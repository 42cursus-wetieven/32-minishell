#ifndef TESTING_H
# define TESTING_H

# include "minishell.h"

void	bug_trap(char *message);
void	print_ctxt_layer(void *layer_addr, size_t index);
void	print_ctxt_stk(t_ctxt *ctxt, char *location);
void	print_rdir_stk(t_vctr	*rdirs);
void	print_pipeline(t_vctr *pipeline, char *context);
void	print_argv(t_arg *arg);
void	print_syntactic_node(t_asn *node, char *context);
void	print_envp(char **envp);

#endif
