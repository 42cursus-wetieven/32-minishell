/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   context_stack.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/01 20:24:40 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/01 20:30:20 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTEXT_STACK_H
# define CONTEXT_STACK_H

# include "minishell.h"

t_error	clear_context_stk(t_ctxt *ctxt);
t_error	add_lex_layer(t_ctxt *ctxt, char new_lex_ctxt);
t_error	add_syn_layer(t_ctxt *ctxt, t_asn *new_syn_ctxt);
void	exit_lex_layer(t_ctxt *ctxt);
void	exit_syn_layer(t_ctxt *ctxt);

#endif
