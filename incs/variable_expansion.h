/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   variable_expansion.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/01 19:22:52 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/01 19:23:06 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef VARIABLE_EXPANSION_H
# define VARIABLE_EXPANSION_H

# include "minishell.h"

t_error	apprehend_variable(t_probe *prb);
t_error	unfold_variable(t_probe *prb);

#endif
