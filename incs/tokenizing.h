#ifndef TOKENIZING_H
# define TOKENIZING_H

# include "minishell.h"

// Parser function pointer and function pointer array structures
//
typedef t_error	(*t_parser)(t_probe *probe);

typedef struct	s_tokn_switch {
	t_parser	fct;
	char		*sep;
}	t_tokn_switch;

t_tokn_switch	*lexicon();
t_lexeme		lex_tokn(char c);

#endif
