#ifndef EXEC_H
# define EXEC_H

# include "minishell.h"

t_error	exec_cmd(t_asn *node, bool lone, int entries, int index);

#endif
