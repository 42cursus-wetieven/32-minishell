#ifndef HEREDOCS_H
# define HEREDOCS_H

# include "minishell.h"

t_error	prompt_heredoc(t_asn *node, char *eof, bool toggle);
t_error	prune_heredocs(t_vctr *rdirs, t_syntagma node_tokn);
t_error	heredoc(t_probe *prb);

#endif
