#ifndef PROMPT_H
# define PROMPT_H

# include "minishell.h"

t_error	interactive_prompt(t_shell *shell, t_probe *probe, char *fixed_prompt);

#endif
