#ifndef ENVIRONMENT_H
# define ENVIRONMENT_H

# include "minishell.h"

char	**envs_list_to_chars(t_node *envs_list);
t_node	*dup_envs_list(t_node *envs_list);
t_node	*sort_envs_list(t_node *envs_list);
int		is_env_var_found(char *var_name, t_node *envs_list);
t_node	*return_env_var(char *var_name, t_node *envs_list);

char	*return_var_value(char *name);
void	change_env_content_val(t_env *env_content, char *new_value);
void	swap_env_content(t_env *curr, t_env *next);
t_env	*init_env_content(char *name, char *value);
int		parse_envs_list(char **envp, t_node **envs_list);

#endif
