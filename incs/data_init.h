/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_init.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/01 20:28:14 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/01 20:29:27 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATA_INIT_H
# define DATA_INIT_H

# include "minishell.h"

void	init_node(t_asn *node);
t_error	new_grouping(t_probe *prb, t_asn *parent, t_syntagma type);
t_error	context_stk_init(t_ctxt *ctxt);
t_error	new_blank_word(t_probe *probe);
t_error	new_blank_arguments(t_arg **arg, t_probe *probe);

#endif
