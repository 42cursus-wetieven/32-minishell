/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_free.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/02 16:38:10 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/02 16:38:11 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATA_FREE_H
# define DATA_FREE_H

# include "minishell.h"

t_error	free_redir_path(void *rdir);
t_error	free_word(void *word_addr);
t_error	free_syntactic_node(void *node_addr);
t_error	free_env_content(void *content);
void	free_double_pointer(char **args);

#endif
