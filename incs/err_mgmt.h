#ifndef ERR_MGMT_H
# define ERR_MGMT_H

# include <stdio.h>
# include <string.h>
# include <errno.h>
# include "minishell.h"

t_error	error(int err_no, t_error type);
int		print_error(char *cmd, char *str, char *err, int ret_status);
t_error	main_p_err(char *filename, char *err, int err_no, t_error type);

#endif
