#ifndef DATA_STORAGE_H
# define DATA_STORAGE_H

# include "minishell.h"

t_error	push_node(t_probe *prb);
t_error	push_redir(t_asn *node, t_rdir *rdir, t_probe *prb);
t_error	push_char(t_probe *probe);
t_error	wording(t_probe *probe);

#endif
