#ifndef GROUPING_H
# define GROUPING_H

# include "minishell.h"

t_error	complete_pipe(t_probe *prb, bool pipe_end);
t_error	pipelin(t_probe *probe);

#endif
