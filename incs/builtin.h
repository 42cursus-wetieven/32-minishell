#ifndef BUILTIN_H
# define BUILTIN_H

/* *** Definitions ********************************************************** */

// For ft_strncmp matches
# define MATCH 0

# include "minishell.h"
# include "err_mgmt.h"

# define NO_VALID_IDF "not a valid identifier"
# define NO_FILE_DIR "No such file or directory\n"

/* *** Exec Structures ****************************************************** */

// Builtin typedef and functor array struct
//
typedef int	(*t_builtin)(char *argv[], t_node **env);

enum	e_builtin {
	CD,
	UNS,
	EXP,
	EXI,
	ECH,
	ENV,
	PWD
};

typedef struct s_builtin_switch {
	char		*call;
	t_builtin	fct;
}	t_builtin_switch;

/* *** Builtins ************************************************************* */

int			do_pwd(char **argv, t_node **envp);
int			do_echo(char **argv, t_node **envp);
int			do_env(char **argv, t_node **envp);
int			do_cd(char **argv, t_node **envp);
int			do_unset(char **argv, t_node **envp);
int			do_export(char **argv, t_node **envp);
int			do_exit(char **argv, t_node **envp);

/* *** Functions ************************************************************ */

int			check_valid_argv(char *argv);
t_builtin	is_builtin(const char *word, int *i);

#endif
