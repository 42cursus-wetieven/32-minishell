#ifndef SIG_HANDLERS_H
# define SIG_HANDLERS_H

# include "minishell.h"

void	during_fork(int signal);
void	interrupt_fork_signals(void);
void	interrupt_main_signals(void);

#endif
