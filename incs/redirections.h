/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirections.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/02 16:37:15 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/02 16:47:16 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef REDIRECTIONS_H
# define REDIRECTIONS_H

# include "minishell.h"

void	close_redirs(t_asn *node, t_vctr *rdirs);
void	connect_redirs(t_asn *node, t_vctr *rdirs);
void	open_redirs(t_vctr *rdirs);
t_error	redirec(t_probe *probe);

#endif
