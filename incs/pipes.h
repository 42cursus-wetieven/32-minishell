#ifndef PIPES_H
# define PIPES_H

# include "minishell.h"

# define LAST_CMD -1
# define WRIT_STDOU STDOUT_FILENO
# define READ_STDIN STDIN_FILENO

t_error	track_and_link_pipes(void *node_addr);

#endif
