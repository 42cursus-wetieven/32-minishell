#ifndef LAUNCHER_H
# define LAUNCHER_H

# include "minishell.h"

t_error	prompt_heredoc(t_asn *node, char *eof, bool toggle);
t_error	run_ass(t_shell *shell);

#endif
