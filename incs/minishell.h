#ifndef MINISHELL_H
# define MINISHELL_H

# include <termios.h>
# include "libft.h"

/* *** Definitions ********************************************************** */

// Colors

# define CYAN		"\033[1;36m"
# define RED		"\033[1;91m"
# define MAGENTA	"\033[1;95m"
# define GREEN		"\033[1;32m"
# define M_GREEN	"\033[32m"
# define YELLOW		"\033[1;93m"
# define RESET		"\033[0m"

// For ft_strncmp matches
# define MATCH 0

// Global error exit
//
extern int	g_err_no;

/* *** Exec Structures ****************************************************** */

// Environment variables structures
//
typedef struct s_env
{
	char			*name;
	char			*value;
}	t_env;

typedef struct s_env_vars
{
	t_node	*envs_list;
	t_node	*loc_envs_list;
}	t_env_vars;

/* *** Parsing Structures *************************************************** */

// Token structures
//
typedef enum e_syntagma
{
	BLNK,
	RDIR,
	COMM,
	/* AND, */
	/* OR, */
	PIPE,
	GRPN,
	MAIN
}	t_syntagma;

typedef enum e_lexeme
{
	HEREDOC,
	SGLQUOT,
	VARIABL,
	DBLQUOT,
	WHITSPC,
	REDIREC,
	PIPELIN,
	/* GROUPIN, */
	/* METACHR, */
	/* NUMERIC, */
	WORD,
}	t_lexeme;

// Command structures - for redirections and executable calls
//
typedef struct s_arg
{
	int		c;
	t_vctr	*v;
	t_node	*envp;
}	t_arg;

typedef struct	s_cmd
{
	t_arg	*arg;
	int		tailpipe[2];
}	t_cmd;

typedef struct s_fd
{
	int	in;
	int	out;
	/* int	err; */
}	t_fd;

typedef enum	e_rdr_typ
{
	HDOC,
	READ,
	WRIT
}	t_rdr_typ;

typedef struct	s_hdoc
{
	bool	active;
	char	*eof;
	int		tailpipe[2];
}	t_hdoc;

typedef struct	s_rdir
{
	t_rdr_typ	type;
	mode_t		mode;
	union
	{
		char	*path;
		char	*eof;
	};
	int			fd;
}	t_rdir;

// Abstract syntax tree structures - to order the execution of our commands
//
typedef struct s_asn t_asn;

// Syntatic node. A container structure for syntatic elements.
//
struct s_asn
{
	t_syntagma	tokn;
	union
	{
		t_vctr	*grp;
		t_cmd	cmd;
	};
	t_fd		fd;
	t_vctr		*rdirs;
	t_hdoc		hdoc;
	bool		first;
	int			errnum;
};

// Probe context struct : To navigate our input string
//
typedef struct	s_ctxt {
	char		lex;
	t_asn		*syn;
	t_vctr		*lex_stk;
	t_vctr		*syn_stk;
}	t_ctxt;

// Probe structure : To scan our input string
//
typedef struct	s_probe {
	t_ctxt		ctxt;
	char		**ptr;
	t_vctr		*word;
	t_arg		*arg;
	t_asn		curr;
	bool		active;
}	t_probe;

// Master shell struct - allowing env data access throughout the stack calls
//
typedef struct s_shell
{
	struct termios	dflt_term;
	t_vctr			*prompt;
	t_asn			mainline;
	t_node			*env;
	t_probe			probe;
	int				fd;
	bool			in_fork;
}	t_shell;

/* *** Functions ************************************************************ */

#endif
