# =============== #
# === TARGETS === #
# =============== #

NAME		=	minishell

## ~~ Folders ~~ ##

DDIR		=	deps/
ODIR		=	objs/

# =============== #
# === SOURCES === #
# =============== #

MAIN_SRCS	=	minishell.c \
				prompt.c \
				err_mgmt.c \
				sig_handlers.c \

PRSG_SRCS	=	tokenizing.c \
				interpreter.c \
				redirections.c \
				context_stack.c \
				grouping.c \
				testing.c \
				variable_expansion.c \
				heredocs.c

DATA_SRCS	=	init.c \
				storage.c \
				free.c \
				access.c \

EXEC_SRCS	=	pwd.c\
				env.c\
				unset.c\
				export.c\
				echo.c\
				cd.c\
				exit.c\
				env_utils.c\
				env_parser.c\
				exec.c \
				launcher.c \
				pipes.c \
				builtin.c \

SRCS = $(MAIN_SRCS) $(PRSG_SRCS) $(DATA_SRCS) $(EXEC_SRCS)

## ~~ Folders ~~ ##

LDIR		=	$(shell find libs -mindepth 1 -maxdepth 1 -type d)

HDIR		=	incs/ $(LDIR:%=%/incs)
SDIR		=	srcs/

# ==================== #
# === PLACEHOLDERS === #
# ==================== #

### ~~~ TARGETS ~~~ ###

OBJS		=	$(SRCS:%.c=$(ODIR)%.o)

DEPS		=	$(SRCS:%.c=$(DDIR)%.d)

LIBS		=	$(shell find $(LDIR) -name '*.a')
LNAMES		=	$(shell find $(LDIR) -name '*.a' -exec basename {} ';')

## ~~ Folders ~~ ##

SUBDIRS		=	$(ODIR) $(DDIR)

### ~~~ SOURCES ~~~ ###

INCS		=	$(shell find $(HDIR) -name '*.h')

## ~~ Folders ~~ ##

SDIR		:=	$(shell find $(SDIR) -mindepth 1 -maxdepth 1 -type d)

vpath %.d $(DDIR)
vpath %.o $(ODIR)
vpath %.a $(LDIR)

vpath %.h $(HDIR)
vpath %.c $(SDIR)

# ====================== #
# === COMPILER SETUP === #
# ====================== #


CC			=	gcc
WRNFL		=	-Wall -Wextra -Werror
OPTFL		=	-O3 -march=native -fno-builtin
DBGFL		=	-g
CFLGS		=	$(WRNFL) $(DBGFL)
DEPFL		=	-MT $@ -MMD -MP -MF $(DDIR)$*.d

ifeq ($(shell uname),Linux)
	CINCS		=	$(addprefix -I, $(HDIR))
	CLDIR		=	$(addprefix -L, $(LDIR))
	CLIBS		=	$(LNAMES:lib%.a=-l%) -lreadline
else
	CINCS		=	$(addprefix -I, $(HDIR)) -I /Users/$(USER)/.brew/opt/readline/include
	CLDIR		=	$(addprefix -L, $(LDIR)) -L /Users/$(USER)/.brew/opt/readline/lib -lreadline
	CLIBS		=	$(LNAMES:lib%.a=-l%)
endif

# ============= #
# === RULES === #
# ============= #

# ~~~ Default ~~~ #

all			:	make_libs $(SUBDIRS) $(NAME)

$(SUBDIRS)	:
				mkdir -p $(SUBDIRS)

# ~~~ Objects Compiling  ~~~ #

$(ODIR)%.o	:	%.c $(DDIR)%.d $(LIBS)
				$(CC) $(CFLGS) $(CINCS) $(DEPFL) -c $< -o $@

# ~~~ Library archiving ~~~ #

#(LIBS)		:	make_libs

make_libs	:
				$(MAKE) -C libs/libft
#				for dir in $(LDIR); do $(MAKE) -C $$dir; done <- use this for multiple libs and versatility

# ~~~ Executables Compiling  ~~~ #

$(NAME)		:	$(OBJS)
				$(CC) $^ -o $@ $(CLDIR) $(CLIBS)

# ~~~ Actions ~~~ #

bonus		:	all

norm		:
				norminette incs srcs

clean		:	$(LDIR)
				rm -rf $(ODIR)
				$(MAKE) -C libs/libft clean

fclean		:	$(LDIR) clean
				$(RM) $(NAME)
				$(RM) $(LIBS)
				rm -rf $(DDIR)
				$(MAKE) -C libs/libft fclean

re			:	fclean all

.PHONY : all make_libs norm clean fclean re bonus

$(DEPS)		:
include $(wildcard $(DEPS))
