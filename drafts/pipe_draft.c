// Par defaut je fais ceci a l'initiatisation d'une commande
new_cmd->fd.in = STDIN_FILENO;
new_cmd->fd.out = STDOUT_FILENO;

// Et des open quand j'ai parsé un nom de fichier en entrée...
cmd->fd.in = open(path_probe->word->data, O_RDONLY);
// ...en sortie
cmd->fd.out = open(path_probe->word->data, open_mode);

// Donc on pourrait faire ceci pour armer les pipe
t_error	init_pipe(t_pipe *pipe)
{
	if (pipe(cmd->pipefd) < 0)
		return (ERROR);
	if (l_cmd->fd.out == STDOUT_FILENO)
		pipe->l_cmd->fd.out = pipefd[1];
	if (r_cmd->fd.in == STDIN_FILENO)
		pipe->r_cmd->fd.in = pipefd[0];
	return (CLEAR);
}

// Usage ainsi
if (lst_iter(pipeline, &init_pipe) != CLEAR)
	

// et du coup au moment de dup suffirait de faire sans plus de tests:
dup2(cmd->fd.in, STDIN_FILENO);
dup2(cmd->fd.out, STDOUT_FILENO);

// Si on a pas armé de redirections osef, on copie STDIN_FILENO
// sur STDIN_FILENO, et ca ne devrait rien changer du coup !
//
// Au pire on pourrait tjrs test si cmd->fd.in et .out sont différents des
// STDIN_FILENO et STDOUT_FILENO avant de faire le dup pour éviter d'en faire
// un dans le vent ou de risquer une erreur de dup.
if (cmd->fd.in != STDIN_FILENO)
	dup2(cmd->fd.in, STDIN_FILENO);
if (cmd->fd.out != STDIN_FILENO)
	dup2(cmd->fd.out, STDOUT_FILENO);

// Le code en serait mieux conceptualisé et plus modulaire et ce serait
// facile a tester en despi pour s'assurer que le principe fonctionne.
