/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   interpreter.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/01 19:23:16 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/02 15:19:20 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "prompt.h"
#include "tokenizing.h"
#include "data_access.h"
#include "context_stack.h"
#include "data_init.h"
#include "data_storage.h"
#include "redirections.h"
#include "grouping.h"
#include "pipes.h"
#include "variable_expansion.h"
#include "err_mgmt.h"
#include "interpreter.h"

t_error	expdvar(t_probe *prb)
{
	t_error	error;

	error = CLEAR;
	if (lex_tokn(prb->ctxt.lex) == VARIABL)
		error = unfold_variable(prb);
	if (error)
		return (error);
	if (!error && **prb->ptr == '$')
		return (apprehend_variable(prb));
	return (error);
}

t_error	quoting(t_probe *prb)
{
	if (lex_tokn(prb->ctxt.lex) == VARIABL)
		return (expdvar(prb));
	if (**prb->ptr == '\\')
	{
		if (prb->ctxt.lex == '\'')
			return (push_char(prb));
		(*prb->ptr)++;
		if (**prb->ptr == '\0')
			return (add_lex_layer(&prb->ctxt, '\\'));
		return (push_char(prb));
	}
	if (prb->ctxt.lex != '\'' && prb->ctxt.lex != '"')
	{
		if (add_lex_layer(&prb->ctxt, **prb->ptr) != CLEAR)
			return (MEM_ALLOC);
		(*prb->ptr)++;
	}
	else if (**prb->ptr == prb->ctxt.lex)
	{
		exit_lex_layer(&prb->ctxt);
		(*prb->ptr)++;
	}
	else
		return (push_char(prb));
	return (CLEAR);
}

static t_error	close_line(t_probe *prb, t_lexeme lexeme)
{
	if (lexeme == HEREDOC)
	{
		if (ft_strcmp(prb->word->data, prb->curr.hdoc.eof) == MATCH)
			prb->active = false;
		else if (prb->curr.hdoc.active)
			ft_dprintf(prb->curr.hdoc.tailpipe[WRIT_STDOU],
				"%s\n", prb->word->data);
		if (new_blank_word(prb) != CLEAR)
			return (MEM_ALLOC);
		return (CLEAR);
	}
	if (prb->ctxt.syn->tokn == PIPE && prb->arg->v->entries)
		return (complete_pipe(prb, true));
	if (lexeme != WORD)
	{
		if (prb->ctxt.lex == '\'' || prb->ctxt.lex == '"')
			vctr_push(prb->word, "\n");
		return (interactive_prompt(shell(), prb, "> "));
	}
	return (push_node(prb));
}

static t_error	close_node(t_probe *prb, t_lexeme lexeme)
{
	if (lexeme == VARIABL || lexeme >= PIPELIN)
		if (wording(prb) != CLEAR)
			return (MEM_ALLOC);
	if (lexeme != WORD && lexeme >= REDIREC)
	{
		if (lexeme == PIPELIN)
		{
			if (**prb->ptr == '\0')
				return (close_line(prb, lexeme));
			else if (prb->arg->v->entries || prb->word->entries)
				return (CLEAR);
		}
		else if (lexeme == REDIREC && prb->word->entries)
			return (CLEAR);
		ft_printf("minishell: syntax error near unexpected token '%c'\n",
			prb->ctxt.lex);
		return (error(2, PARSE));
	}
	if (**prb->ptr == '\0')
		return (close_line(prb, lex_tokn(prb->ctxt.lex)));
	return (CLEAR);
}

t_error	interpreter(t_probe *prb)
{
	t_error		error;
	t_lexeme	cursor;
	t_lexeme	lexeme;

	error = CLEAR;
	while (!error && **prb->ptr && prb->active)
	{
		cursor = lex_tokn(**prb->ptr);
		lexeme = lex_tokn(prb->ctxt.lex);
		if (cursor == WORD || lexeme == HEREDOC
			|| (lexeme <= DBLQUOT && cursor > lexeme && cursor < DBLQUOT))
			error = push_char(prb);
		else
		{
			if (cursor >= PIPELIN)
				error = close_node(prb, lexeme);
			if (!error)
				error = (lexicon()[cursor].fct)(prb);
		}
	}
	if (prb->active && !error)
		error = close_node(prb, lex_tokn(prb->ctxt.lex));
	if (!error && prb->ctxt.syn->tokn == MAIN && prb->ctxt.lex == '\0')
			prb->active = false;
	return (error);
}
