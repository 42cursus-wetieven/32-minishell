/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   variable_expansion.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/01 19:22:55 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/02 14:30:54 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "tokenizing.h"
#include "data_storage.h"
#include "context_stack.h"
#include "environment.h"
#include "variable_expansion.h"

static t_error	print_last_err_no(t_probe *prb)
{
	char	*value;

	if (g_err_no != 0)
	{
		value = ft_itoa(g_err_no);
		if (value && str_to_vctr(prb->word, value) != CLEAR)
			return (MEM_ALLOC);
		free(value);
	}
	else if (vctr_push(prb->word, "0") != CLEAR)
		return (MEM_ALLOC);
	*prb->ptr = *prb->ptr + 2;
	return (CLEAR);
}

t_error	apprehend_variable(t_probe *prb)
{
	if (*(*prb->ptr + 1) == '?')
		return (print_last_err_no(prb));
	if (add_lex_layer(&prb->ctxt, '$') != CLEAR)
		return (MEM_ALLOC);
	if (push_char(prb) != CLEAR)
		return (MEM_ALLOC);
	if (**prb->ptr == '\0' || lex_tokn(**prb->ptr) != WORD)
	{
			exit_lex_layer(&prb->ctxt);
			if (lex_tokn(prb->ctxt.lex) != HEREDOC
				&& (**prb->ptr == '\'' || **prb->ptr == '\"'))
				vctr_remove(prb->word, prb->word->entries - 1, NULL);
	}
	return (CLEAR);
}

t_error	unfold_variable(t_probe *prb)
{
	size_t	i;
	char	*ptr;
	char	end_chr;
	char	*value;

	ptr = ft_strchr(prb->word->data, '$') + 1;
	while (*ptr != '\0' && lex_tokn(*ptr) == WORD)
		ptr++;
	end_chr = *ptr;
	*ptr = '\0';
	value = return_var_value(ft_strchr(prb->word->data, '$') + 1);
	*ptr = end_chr;
	i = ptr - (char *)prb->word->data;
	while (*(char *)vctr_entry(prb->word, --i) != '$')
		vctr_remove(prb->word, i, NULL);
	vctr_remove(prb->word, i, NULL);
	if (value)
		while (*value)
			if (vctr_insert(prb->word, i++, value++) != CLEAR)
				return (MEM_ALLOC);
	exit_lex_layer(&prb->ctxt);
	/* if (**prb->ptr != '"' && prb->ctxt.lex != '"') */
	/* 	return (MEM_ALLOC); */
	return (CLEAR);
}
