#include "interpreter.h"
#include "data_storage.h"
#include "redirections.h"
#include "grouping.h"
#include "data_access.h"
#include "heredocs.h"
#include "tokenizing.h"

t_tokn_switch	*lexicon(void)
{
	static t_tokn_switch	lexical_switchboard[] = {
	[HEREDOC] = {.fct = &heredoc, .sep = "\'"},
	[SGLQUOT] = {.fct = &quoting, .sep = "\'\\"},
	[VARIABL] = {.fct = &expdvar, .sep = "$"},
	[DBLQUOT] = {.fct = &quoting, .sep = "\""},
	[WHITSPC] = {.fct = &wording, .sep = " \t\n\v\f\r"},
	[REDIREC] = {.fct = &redirec, .sep = "<>"},
	[PIPELIN] = {.fct = &pipelin, .sep = "|&"},
	/* [GROUPIN] = {.fct = &metachr, .sep = "()"}, */
	/* [METACHR] = {.fct = &metachr, .sep = "!*?[];(){}"}, */
	/* [NUMERIC] = {.fct = &numeric, .sep = "0123456789"}, */
	};

	return (lexical_switchboard);
}

t_lexeme	lex_tokn(char c)
{
	int			i;
	t_lexeme	tokn_type;

	if (shell()->probe.curr.hdoc.eof)
		tokn_type = HEREDOC;
	else
		tokn_type = SGLQUOT;
	while (tokn_type < WORD)
	{
		i = 0;
		while (lexicon()[tokn_type].sep[i] != '\0')
		{
			if (c == lexicon()[tokn_type].sep[i])
				return (tokn_type);
			i++;
		}
		tokn_type++;
	}
	return (tokn_type);
}
