#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "testing.h" // TESTING

static	char	*syntax_key(t_syntagma key)
{
	static char	*syntax[] = {
	[PIPE] = "PIPE",
	[COMM] = "COMM",
	[RDIR] = "RDIR",
	[BLNK] = "BLNK",
	[MAIN] = "MAIN",
	};

	return (syntax[key]);
}

// Just set a breakpoint to that fella and ensnare those bugs
//
void	bug_trap(char *message)
{
	/* (void)message; */
	ft_printf("%s< << ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ >> >\n", YELLOW);
	ft_printf("%s\n", message);
	perror(strerror(errno));
	ft_printf("< << ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ >> >\n%s", RESET);
}

void	print_ctxt_stk(t_ctxt *ctxt, char *location)
{
	(void)ctxt;
	(void)location;
	/* static char	*void_str = ""; */
	/* size_t	i; */
	/* char	*lex; */
	/* t_asn	*syn; */

	/* ft_printf("<< CTXT STCK >>\n"); */
	/* if (location) */
	/* 	ft_printf("< %s >\n", location); */
	/* ft_printf("SYN    |    LEX\n"); */
	/* for (i = 0; i < ctxt->lex_stk->entries || i < ctxt->syn_stk->entries ; i++) */
	/* { */
	/* 	syn = vctr_entry(ctxt->syn_stk, i); */
	/* 	if (syn) */
	/* 		ft_printf("%-6s | ", syntax_key(syn->tokn)); */
	/* 	else */
	/* 		ft_printf("%-6s | ", void_str); */
	/* 	lex = vctr_entry(ctxt->lex_stk, i); */
	/* 	if (lex) */
	/* 		ft_printf("%6s\n", lex); */
	/* 	else */
	/* 		ft_printf("%6s\n", void_str); */
	/* } */
	/* ft_printf("<< ~~~~~~~~~ >>\n"); */
}

void	print_rdir_stk(t_vctr	*rdirs)
{
	t_rdir		*rdir;
	static char	*type[] = {
	[READ] = "read",
	[WRIT] = "writ",
	[HDOC] = "hdoc"
	};
	size_t		i;

	ft_printf("|>>>>>>>>>>>>>>>>>>\n");
	ft_printf("| REDIRECTION STACK\n");
	ft_printf("| TYPE | PATH / EOF | FD\n");
	rdir = rdirs->data;
	for (i = 0; i < rdirs->entries; i++)
	{
		ft_printf("| %s | %s | %i\n", type[rdir->type], rdir->path, rdir->fd);
		rdir++;
	}
	ft_printf("|>>>>>>>>>>>>>>>>>>\n");
}

void	print_pipeline(t_vctr *pipeline, char *context)
{
	size_t	i;

	if (context)
		ft_printf("%s< %s >%s\n", GREEN, context, RESET);
	ft_printf("Pipeline entries = %lu\n", pipeline->entries);
	ft_printf("%s======================\n%s", GREEN, RESET);
	for (i = 0; i < pipeline->entries; i++)
		print_syntactic_node(vctr_entry(pipeline, i), context);
	ft_printf("%s======================\n%s", GREEN, RESET);
}

void	print_argv(t_arg *arg)
{
	char	*word;
	char	**ptr;
	size_t	i;

	ft_printf("%s*** arg->v words : ***\n", MAGENTA);
	ft_printf("address %p\n", &arg->v);
	ptr = arg->v->data;
	for (i = 0; i < arg->v->entries; i++)
	{
		word = *(ptr++);
		ft_printf("%i - %s\n", i, word);
		ft_printf("address %p\n", word);
	}
	/* ft_printf("%s", RESET); */
}

void	print_envp(char **envp)
{
	ft_printf("%s*** envp values : ***\n", MAGENTA);
	while (*envp)
	{
		ft_printf("%s\n", *envp);
		envp++;
	}
	ft_printf("%s*********************\n\n", MAGENTA);
}

void	print_syntactic_node(t_asn *node, char *context)
{
	if (node->tokn == RDIR)
		ft_printf("%s", YELLOW);
	else if (node->tokn == COMM)
		ft_printf("%s", CYAN);
	else if (node->tokn >= PIPE)
		ft_printf("%s", GREEN);
	else
		ft_printf("%s", RED);
	ft_printf("[   %s CONTAINER   ]\n", syntax_key(node->tokn));
	ft_printf("address %p\n", node);
	if (context)
		ft_printf("< %s >\n", context);
	if (node->tokn >= PIPE)
		print_pipeline(node->grp, NULL);
	else if (node->tokn == COMM)
	{
		ft_printf("----------------------\n");
		ft_printf("fd.in = %i / fd.out = %i\n", node->fd.in, node->fd.out);
		print_argv(node->cmd.arg);
		if (node->rdirs)
			print_rdir_stk(node->rdirs);
		ft_printf("----------------------\n");
	}
	else if (node->tokn == RDIR)
	{
		ft_printf("----------------------\n");
		print_rdir_stk(node->rdirs);
		ft_printf("----------------------\n");
	}
	ft_printf("%s", RESET);
}
