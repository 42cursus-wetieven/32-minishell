#include <termios.h>
#include "libft.h"
#include "minishell.h"
#include "tokenizing.h"
#include "prompt.h"
#include "data_init.h"
#include "data_storage.h"
#include "data_access.h"
#include "data_free.h"
#include "environment.h"

int g_err_no = 0;

static void	free_exit(t_node *envs_list, char *message, int exit_val)
{
	if (envs_list)
		free_lst(&envs_list, free_env_content);
	if (message)
		ft_putendl_fd(message, STDERR_FILENO);
	exit(exit_val);
}
static t_error	check_std(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	if (isatty(STDIN_FILENO) && isatty(STDOUT_FILENO) && isatty(STDERR_FILENO))
		return (CLEAR);
	return (ERROR);
}

int	main(int argc, char **argv, char **envp)
{
	t_probe	*probe;

	if (check_std(argc, argv) != CLEAR)
		free_exit(NULL, "File descriptors occupied!", EXIT_FAILURE);
	if (!parse_envs_list(envp, &shell()->env))
		free_exit(NULL, "Parsing went wrong", EXIT_FAILURE);
	probe = &shell()->probe;
	init_node(&probe->curr);
	tcgetattr(STDIN_FILENO, &shell()->dflt_term);
	interactive_prompt(shell(), probe, NULL); // Protect this against mem saturation
	/* if (interactive_prompt(&shell, &probe, NULL) == MEM_ALLOC) */
	/* 	return (msh_shutdown(error)); // Try to relaunch */
}
