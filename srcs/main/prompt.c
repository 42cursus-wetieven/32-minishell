#include <stdio.h>
#include <limits.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "sig_handlers.h"
#include "data_init.h"
#include "data_storage.h"
#include "data_free.h"
#include "tokenizing.h"
#include "interpreter.h"
#include "context_stack.h"
#include "testing.h" //TESTING
#include "launcher.h"
#include "builtin.h"
#include "prompt.h"

static t_error	compose_prompt(t_vctr *prompt, char *username, char *cwd)
{
	t_error	error;

	error = CLEAR;
	error = str_to_vctr(prompt, CYAN);
	if (!error)
		error = str_to_vctr(prompt, ft_strrchr(cwd, (int) '/') + 1);
	if (!error)
		error = str_to_vctr(prompt, MAGENTA);
	if (!error)
		error = str_to_vctr(prompt, " (");
	if (!error)
		error = str_to_vctr(prompt, username);
	if (!error)
		error = str_to_vctr(prompt, "@minishell) ");
	if (!error)
		error = str_to_vctr(prompt, GREEN);
	if (!error)
		error = str_to_vctr(prompt, "$ ");
	if (!error)
		error = str_to_vctr(prompt, RESET);
	return (error);
}

static t_error	refresh_prompt(t_vctr **prompt)
{
	char	*username;
	char	cwd[PATH_MAX];

	if (!*prompt)
	{
		if (vctr_init(prompt, sizeof(char), 16) != CLEAR)
			return (MEM_ALLOC);
	}
	else
		vctr_recycle(*prompt, NULL);
	getcwd(cwd, PATH_MAX);
	username = getenv("USER");
	if (!username)
		username = "guest";
	return (compose_prompt(*prompt, username, cwd));
}

static t_error	probe_reset(t_shell *shell, t_probe *prb)
{
	if (lex_tokn(prb->ctxt.lex) == HEREDOC)
	{
		prb->active = true;
		return (new_blank_word(prb));
	}
	if (vctr_init(&shell->mainline.grp, sizeof(t_asn), 2) != CLEAR) 
		return (MEM_ALLOC);
	if (clear_context_stk(&prb->ctxt) != CLEAR)
		return (MEM_ALLOC);
	if (prb->arg != NULL)
	{
		vctr_recycle(prb->arg->v, free_word);
		prb->arg->c = 0;
	}
	else
		if (new_blank_arguments(&prb->arg, prb) != CLEAR)
			return (MEM_ALLOC);
	prb->active = true;
	return (CLEAR);
}

static void	save_history(char *line)
{
	if (line && *line)
		add_history(line);
}

t_error	interactive_prompt(t_shell *shell, t_probe *probe, char *fixed_prompt)
{
	t_error	error;
	char	*input;
	char	*input_start;

	error = CLEAR;
	while (error != MEM_ALLOC)
	{
		interrupt_main_signals();
		if (fixed_prompt)
			input = readline(fixed_prompt);
		else
		{
			if (refresh_prompt(&shell->prompt) != CLEAR)
				return (MEM_ALLOC);
			input = readline(shell->prompt->data);
		}
		if (!input) // THIS is the CTRL-D intercept !
		{
			/* if (fixed_prompt) */
			/* 	return // ERR MSG and break */
			ft_putendl_fd("exit", 1);
			return ((t_error)do_exit);
		}
		save_history(input);
		input_start = input;
		probe->ptr = &input;
		if (!probe->active)
			if (probe_reset(shell, probe) != CLEAR)
				return (MEM_ALLOC);
		error = interpreter(probe);
		free(input_start);
		if (error)
		{
			probe->active = false;
			bug_trap("INTERPRETER ERROR"); // TESTING
		}
		if (fixed_prompt && !probe->active)
			break ;
		if (!fixed_prompt) // May get the second condition out
		{
			/* print_syntactic_node(&shell->mainline, "< commd stk @ prompt >"); // TESTING */
			if (!error)
				run_ass(shell);
			if (shell->mainline.grp && shell->mainline.grp->entries)
				free_syntactic_node(&shell->mainline);
		}
	}
	return (error);
}
//	free_env_vars(&shell.env);
