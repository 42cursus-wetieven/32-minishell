#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <signal.h>
#include "libft.h"
#include "minishell.h"
#include "data_access.h"

void	during_fork(int signal)
{
	if (signal == SIGINT)
	{
		ft_putstr_fd("\n", 1);
		rl_on_new_line();
		rl_replace_line("", 0);
	}
}
/*
void	ctrlbs_fork_handler(int signal)
{
	(void)signal;
	if (signal == SIGINT)
	{
		ft_putstr_fd("^\\Quit: 3", 1);
		rl_replace_line("", 0);
		// we need to update the error status value = 131
	}
}
*/
void	interrupt_fork_signals(void)
{
	// After thinking about it, I don't think we need to get back to the
	// previous terminal config cuz i'm displaying the necessary outputs for
	// the child processes in the handlers
	/*
	struct termios	*term_default;
	//term_default = we need to get the default config that we saved earlier
	tcsetattr(STDIN_FILENO, 0, term_default);
	*/
	tcsetattr(STDIN_FILENO, TCSANOW, &shell()->dflt_term);
	signal(SIGINT, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
}

void	main_handler(int signal)
{
	if (signal == SIGINT)
	{
		/* g_err_no = 1; //a verifier maybe */
		ft_putstr_fd("\n", 1);
		rl_on_new_line();
		rl_replace_line("", 0);
		rl_redisplay();
	}
}

void	interrupt_main_signals(void)
{
	struct termios	term_interr;

	tcgetattr(STDIN_FILENO, &term_interr);
	term_interr.c_lflag |= ECHOCTL;
	tcsetattr(STDIN_FILENO, TCSANOW, &term_interr);
	signal(SIGINT, main_handler);
	signal(SIGQUIT, SIG_IGN);
}
