#include "err_mgmt.h"

t_error	error(int err_no, t_error type)
{
	g_err_no = err_no;
	return (type);
}

int	print_error(char *cmd, char *str, char *err, int ret_status)
{
	ft_dprintf(STDERR_FILENO, "minishell: ");
	if (cmd)
		ft_dprintf(STDERR_FILENO, "%s: ", cmd);
	if (str)
		ft_dprintf(STDERR_FILENO, "%s: ", str);
	if (err)
		ft_dprintf(STDERR_FILENO, "%s\n", err);
	return (ret_status);
}

t_error	main_p_err(char *filename, char *err, int err_no, t_error type)
{
	ft_dprintf(STDERR_FILENO, "minishell: ");
	if (filename)
		ft_dprintf(STDERR_FILENO, "%s: ", filename);
	if (err)
		ft_dprintf(STDERR_FILENO, "%s\n", err);
	return (error(err_no, type));
}
