#include "context_stack.h"
#include "tokenizing.h"
#include "data_init.h"
#include "data_storage.h"
/* #include "testing.h" //TESTING */
#include "grouping.h"

t_error	complete_pipe(t_probe *prb, bool pipe_end)
{
	if (!pipe_end)
		return (push_node(prb));
	exit_lex_layer(&prb->ctxt);
	if (push_node(prb) != CLEAR)
		return (MEM_ALLOC);
	exit_syn_layer(&prb->ctxt);
	return (CLEAR);
}

static t_error	build_pipe(t_probe *prb)
{
	t_asn	*prv_node;
	t_vctr	*parent_grp;

	if (push_node(prb))
		return (MEM_ALLOC);
	prv_node = vctr_entry(prb->ctxt.syn->grp, prb->ctxt.syn->grp->entries - 1);
	if (!prv_node) // No lonely | allowed
		return (PARSE);
	parent_grp = prb->ctxt.syn->grp;
	if (new_grouping(prb, prb->ctxt.syn, PIPE) != CLEAR)
		return (MEM_ALLOC);
	if (vctr_push(prb->ctxt.syn->grp, prv_node) != CLEAR)
		return (MEM_ALLOC);
	vctr_remove(parent_grp, parent_grp->entries - 2, NULL);
	if (add_lex_layer(&prb->ctxt, '|') != CLEAR)
		return (MEM_ALLOC);
	return (CLEAR);
}

t_error	pipelin(t_probe *prb)
{
	(*prb->ptr)++;
	if (*(*prb->ptr - 1) == '|')
	{
		if (**prb->ptr == '&')
			(*prb->ptr)++;
		if (prb->ctxt.syn->tokn != PIPE)
			return (build_pipe(prb));
		return (complete_pipe(prb, false));
	}
	/* else if (*(*prb->ptr - 1) == '(') */
	/* 		return (open_group(prb)); */
	/* else if (*(*prb->ptr - 1) == ')') */
	/* 		return (close_group(prb)); */
	return (PARSE);
}
