#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "context_stack.h"
#include "data_access.h"
#include "prompt.h"
#include "data_free.h"
#include "builtin.h"
#include "heredocs.h"
#include "testing.h" // TESTING
#include "data_storage.h"
#include "pipes.h"

static	t_error	wait_hdocs(int pid, int toclose)
{
	bug_trap("HDOC FORK");
	if (waitpid(pid, &pid, 0) == -1)
		return (ERROR);
	if (WIFEXITED(pid))
		g_err_no = WEXITSTATUS(pid);
	if (close(toclose) == -1)
		return (ERROR);
	return (CLEAR);
}

t_error	prompt_heredoc(t_asn *node, char *eof, bool toggle)
{
	int	pid;

	if (toggle == true)
	{
		pid = fork();
		if (pid > 0)
			return (wait_hdocs(pid, node->hdoc.tailpipe[WRIT_STDOU]));
	}
	if (add_lex_layer(&shell()->probe.ctxt, '\'') != CLEAR)
		return (MEM_ALLOC);
	node->hdoc.active = toggle;
	node->hdoc.eof = eof;
	shell()->probe.active = false;
	shell()->probe.curr = *node;
	if (interactive_prompt(shell(), &shell()->probe, "> ") != CLEAR)
		return (MEM_ALLOC);
	shell()->probe.curr.hdoc.active = false;
	shell()->probe.curr.hdoc.eof = NULL;
	exit_lex_layer(&shell()->probe.ctxt);
	//close(node->hdoc.tailpipe[WRIT_STDOU]);
	//close(node->hdoc.tailpipe[READ_STDIN]);
	if (toggle == true && pid == 0)
		do_exit(NULL, NULL);
	return (CLEAR);
}

t_error	prune_heredocs(t_vctr *rdirs, t_syntagma node_tokn)
{
	bool	failed_rdir;
	size_t	i;
	t_rdir	*rdir;

	failed_rdir = false;
	i = 0;
	while (i < rdirs->entries)
	{
		rdir = vctr_entry(rdirs, i);
		if (rdir->type != HDOC)
			failed_rdir = true;
		else if (i < rdirs->entries - 1 || failed_rdir || node_tokn <= RDIR)
		{
			if (prompt_heredoc(&shell()->probe.curr, rdir->eof, false) != CLEAR)
				return (MEM_ALLOC);
			vctr_remove(rdirs, i--, free_redir_path);
		}
		i++;
	}
	return (CLEAR);
}

// If we end up here, it means that we've stumbled upon a "'" while parsing
// a variable in a heredoc. So we should just keep on pushing, pushing the
// char away...
//
t_error	heredoc(t_probe *prb)
{
	return (push_char(prb));
}
