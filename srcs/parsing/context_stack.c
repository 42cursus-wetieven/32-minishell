/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   context_stack.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/01 20:26:27 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/01 20:32:46 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "data_init.h"
#include "data_access.h"
#include "data_storage.h"
#include "context_stack.h"

t_error	add_lex_layer(t_ctxt *ctxt, char new_lex_ctxt)
{
	if (vctr_push(ctxt->lex_stk, &new_lex_ctxt))
		return (MEM_ALLOC);
	ctxt->lex = new_lex_ctxt;
	return (CLEAR);
}

t_error	add_syn_layer(t_ctxt *ctxt, t_asn *new_syn_ctxt)
{
	if (vctr_push(ctxt->syn_stk, new_syn_ctxt))
		return (MEM_ALLOC);
	ctxt->syn = new_syn_ctxt;
	return (CLEAR);
}

void	exit_lex_layer(t_ctxt *ctxt)
{
	if (ctxt->lex_stk->entries <= 1)
		return ;
	vctr_remove(ctxt->lex_stk, ctxt->lex_stk->entries - 1, NULL);
	ctxt->lex = *(char *)vctr_tail(ctxt->lex_stk);
}

void	exit_syn_layer(t_ctxt *ctxt)
{
	if (ctxt->syn_stk->entries <= 1)
		return ;
	vctr_remove(ctxt->syn_stk, ctxt->syn_stk->entries - 1, NULL);
	ctxt->syn = (t_asn *)vctr_tail(ctxt->syn_stk);
}

t_error	clear_context_stk(t_ctxt *ctxt)
{
	if (!ctxt->lex_stk || !ctxt->syn_stk)
	{
		if (context_stk_init(ctxt) != CLEAR)
			return (MEM_ALLOC);
	}
	else
	{
		if (vctr_recycle(ctxt->lex_stk, NULL) != CLEAR)
			return (MEM_ALLOC);
		if (vctr_recycle(ctxt->syn_stk, NULL) != CLEAR)
			return (MEM_ALLOC);
	}
	if (add_lex_layer(ctxt, '\0') != CLEAR)
		return (MEM_ALLOC);
	if (add_syn_layer(ctxt, &shell()->mainline) != CLEAR)
		return (MEM_ALLOC);
	return (CLEAR);
}
