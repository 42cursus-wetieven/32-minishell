/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirections.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/02 16:49:21 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/02 16:53:19 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include "tokenizing.h"
#include "context_stack.h"
#include "interpreter.h"
#include "data_init.h"
#include "data_access.h"
#include "data_storage.h"
#include "err_mgmt.h"
#include "data_free.h"
#include "redirections.h"

void	close_redirs(t_asn *node, t_vctr *rdirs)
{
	t_rdir	*remaining_rdir;

	if (node->fd.in != STDIN_FILENO)
		close(node->fd.in);
	if (node->fd.out != STDOUT_FILENO)
		close(node->fd.out);
	if (rdirs)
	{
		remaining_rdir = vctr_tail(rdirs);
		if (remaining_rdir && remaining_rdir->type != HDOC)
		{
			if (shell()->in_fork == false)
			{
				open(remaining_rdir->path, remaining_rdir->mode);
				g_err_no = errno;
				main_p_err(remaining_rdir->path, strerror(errno), errno, PARSE);
				if (g_err_no == 13)
					g_err_no = 1;
			}
		}
		vctr_iter(node->rdirs, free_redir_path);
		vctr_exit(node->rdirs);
	}
}

void	connect_redirs(t_asn *node, t_vctr *rdirs)
{
	size_t	i;
	t_rdir	*rdir;

	i = 0;
	while (i < rdirs->entries)
	{
		rdir = vctr_entry(rdirs, i);
		if (rdir->type == READ)
		{
			if (node->fd.in != STDIN_FILENO && node->fd.in != ERROR)
				close(node->fd.in);
			node->fd.in = rdir->fd;
		}
		else if (rdir->type == WRIT)
		{
			if (node->fd.out != STDOUT_FILENO && node->fd.out != ERROR)
				close(node->fd.out);
			node->fd.out = rdir->fd;
		}
		if (rdir->type != HDOC && rdir->fd != ERROR)
		{
			vctr_remove(rdirs, i--, free_redir_path);
		}
		i++;
	}
}

void	open_redirs(t_vctr *rdirs)
{
	size_t	i;
	t_rdir	*rdir;

	i = 0;
	while (i < rdirs->entries)
	{
		rdir = vctr_entry(rdirs, i);
		if (rdir->type != HDOC)
			rdir->fd = open(rdir->path, rdir->mode, 0644);
		if (rdir->fd == ERROR)
			break ;
		i++;
	}
	while (++i < rdirs->entries)
	{
		rdir = vctr_entry(rdirs, i);
		if (rdir->type != HDOC)
		{
			vctr_remove(rdirs, i, free_redir_path);
			i--;
		}
	}
}

static t_error	parse_redir(t_rdir *rdir, t_probe *prb)
{
	if (rdir->type == READ && **prb->ptr == '<')
	{
		rdir->type = HDOC;
		rdir->fd = STDIN_FILENO;
		(*prb->ptr)++;
	}
	else if (rdir->type == WRIT && **prb->ptr == '>')
	{
		rdir->mode = O_WRONLY | O_CREAT | O_APPEND;
		(*prb->ptr)++;
	}
	return (interpreter(prb));
}

t_error	redirec(t_probe *prb)
{
	t_error	error;
	t_rdir	rdir;

	if (add_lex_layer(&prb->ctxt, **prb->ptr) != CLEAR)
		return (MEM_ALLOC);
	if (**prb->ptr == '<')
	{
		rdir.type = READ;
		rdir.mode = O_RDONLY;
	}
	if (*((*prb->ptr)++) == '>')
	{
		rdir.type = WRIT;
		rdir.mode = O_WRONLY | O_CREAT | O_TRUNC;
	}
	error = parse_redir(&rdir, prb);
	if (error)
		return (error);
	if (push_redir(&prb->curr, &rdir, prb) != CLEAR)
		return (MEM_ALLOC);
	exit_lex_layer(&prb->ctxt);
	vctr_recycle(prb->word, NULL);
	if (**prb->ptr != '\0')
		prb->active = true;
	return (CLEAR);
}
