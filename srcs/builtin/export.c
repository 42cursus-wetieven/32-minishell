#include "environment.h"
#include "data_free.h"
#include "builtin.h"

int	add_export(char *str_var, t_node *envs_list, int sign_i)
{
	t_node	*temp_env;
	char	*name;
	char	*value;

	name = ft_substr(str_var, 0, sign_i);
	value = ft_substr(str_var, sign_i + 1, ft_strlen(str_var) - (sign_i + 1));
	temp_env = return_env_var(name, envs_list);
	if (temp_env)
	{
		if (value)
			change_env_content_val(temp_env->ctnt, value);
	}
	else
		lst_queue(&envs_list, new_node(init_env_content(name, value)));
	free(name);
	free(value);
	return (0);
}

int	append_export(char *str_var, t_node *envs_list, int sign_i)
{
	t_node	*temp_env;
	char	*name;
	char	*value;
	char	*temp_value;

	name = ft_substr(str_var, 0, sign_i);
	value = ft_substr(str_var, sign_i + 2, ft_strlen(str_var) - (sign_i + 2));
	temp_env = return_env_var(name, envs_list);
	if (!temp_env)
		lst_queue(&envs_list, new_node(init_env_content(name, value)));
	else
	{
		temp_value = ft_strjoin(((t_env *)temp_env->ctnt)->value, value);
		change_env_content_val(temp_env->ctnt, temp_value);
		free(temp_value);
	}
	free(name);
	free(value);
	return (0);
}

int	execute_export(char *str_var, t_node *envs_list)
{
	t_node	*temp_env;
	int		i;

	i = 0;
	while (*(str_var + i))
	{
		if (*(str_var + i) == '+' || *(str_var + i) == '-')
		{
			if (*(str_var + i) == '+' && *(str_var + i + 1) == '=')
				return (append_export(str_var, envs_list, i));
			else
				return (print_error("export", str_var, NO_VALID_IDF, 1));
		}
		else if (*(str_var + i) == '=')
			return (add_export(str_var, envs_list, i));
		i++;
	}
	if (*(str_var + i) == '\0')
	{
		temp_env = return_env_var(str_var, envs_list);
		if (!temp_env)
		lst_queue(&envs_list, new_node(init_env_content(str_var, NULL)));
	}
	return (0);
}

void	display_export(t_node *envs_list)
{
	t_node	*envs_sorted;
	t_env	*env_content;

	envs_sorted = sort_envs_list(envs_list);
	while (envs_sorted)
	{
		env_content = (t_env *)envs_sorted->ctnt;
		if (env_content->name)
			ft_dprintf(STDOUT_FILENO, "declare -x %s=", env_content->name);
		if (env_content->value)
			ft_dprintf(STDOUT_FILENO, "\"%s\"\n", env_content->value);
		envs_sorted = envs_sorted->next;
	}
	free_lst(&envs_sorted, free_env_content);
}

int	check_valid_argv_export(char *str_var)
{
	char	*argv_name;
	int		ret_val;
	int		i;

	i = 0;
	ret_val = 0;
	while (*(str_var + i))
	{
		if (*(str_var + i) == '+' || *(str_var + i) == '=')
			break ;
		i++;
	}
	argv_name = ft_substr(str_var, 0, i);
	if (check_valid_argv(argv_name) == 1)
		ret_val = 1;
	free(argv_name);
	return (ret_val);
}

int	do_export(char **argv, t_node **envp)
{
	int	i;
	int	ret_val;

	ret_val = 0;
	i = 1;
	if (!*(argv + i))
		display_export(*envp);
	while (*(argv + i))
	{
		if (check_valid_argv_export(*(argv + i)) == 0)
			ret_val = print_error("export", *(argv + i), NO_VALID_IDF, 1);
		else
		{
			if (ret_val == 1)
				execute_export(*(argv + i), *envp);
			else
				ret_val = execute_export(*(argv + i), *envp);
		}
		i++;
	}
	return (ret_val);
}
