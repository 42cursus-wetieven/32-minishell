#include "builtin.h"

int	do_env(char **argv, t_node **envp)
{
	t_node	*envs_list;
	t_env	*env_content;

	envs_list = *envp;
	if (*(argv + 1))
	{
		ft_dprintf(STDERR_FILENO, "env: %s : %s\n", *(argv + 1), NO_FILE_DIR);
		return (127);
	}
	while (envs_list)
	{
		env_content = (t_env *)envs_list->ctnt;
		if (env_content->name)
			ft_dprintf(1, "%s=", env_content->name);
		if (env_content->value)
			ft_dprintf(1, "%s\n", env_content->value);
		envs_list = envs_list->next;
	}
	return (0);
}
