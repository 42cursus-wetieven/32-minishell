#include "builtin.h"

int	check_valid_argv(char *argv)
{
	int	i;
	
	i = 0;
	if (!ft_isalpha(*(argv + i)) && *(argv + i) != '_')
		return (0);
	while (*(argv + i))
	{
		if (!ft_isalnum(*(argv + i)) && *(argv + i) != '_')
			return (0);
		i++;
	}
	return (1);
}

t_builtin	is_builtin(const char *word, int *i)
{
	static t_builtin_switch	swtcbrd[] = {
	[CD] = {.call = "cd", .fct = &do_cd},
	[PWD] = {.call = "pwd", .fct = &do_pwd},
	[ENV] = {.call = "env", .fct = &do_env},
	[ECH] = {.call = "echo", .fct = &do_echo},
	[EXI] = {.call = "exit", .fct = &do_exit},
	[UNS] = {.call = "unset", .fct = &do_unset},
	[EXP] = {.call = "export", .fct = &do_export},
	};

	*i = 0;
	while (*i < 7)
	{
		if (ft_strcmp(swtcbrd[*i].call, word) == MATCH)
			return (swtcbrd[*i].fct);
		(*i)++;
	}
	return (NULL);
}
