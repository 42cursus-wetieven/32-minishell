#include "environment.h"
#include "data_free.h"
#include "builtin.h"

void	change_env_content_name(t_env *env_content, char *new_name)
{
	if (env_content->name)
		free(env_content->name);
	if (new_name)
		env_content->name = ft_strdup(new_name);
	else
		env_content->name = NULL;
}

void	offset_envs_list(t_node *envs_list)
{
	t_node	*ptr_env;
	t_env	*new_content;

	ptr_env = envs_list;
	while (ptr_env && ptr_env->next)
	{
		new_content = (t_env *)ptr_env->next->ctnt;
		change_env_content_name(ptr_env->ctnt, new_content->name);
		change_env_content_val(ptr_env->ctnt, new_content->value);
		ptr_env = ptr_env->next;
	}
	change_env_content_name(ptr_env->ctnt, NULL);
	change_env_content_val(ptr_env->ctnt, NULL);
}

void	unset_env_var(char *var_name, t_node **envs_list)
{
	t_node	*ptr_env;
	t_node	*temp_env;
	t_env	*env_content;

	ptr_env = *envs_list;
	env_content = (t_env *)ptr_env->ctnt;
	if (ft_strcmp(env_content->name, var_name) == 0)
	{
		offset_envs_list(*envs_list);
		return ;
	}
	while (ptr_env && ptr_env->next)
	{
		temp_env = ptr_env->next;
		env_content = (t_env *)temp_env->ctnt;
		if (ft_strcmp(env_content->name, var_name) == 0)
		{
			ptr_env->next = temp_env->next;
			free_env_content(temp_env->ctnt);
			free(temp_env);
			return ;
		}
		ptr_env = ptr_env->next;
	}
}

int	do_unset(char **argvs, t_node **envs_list)
{
	int	i;
	int	ret_val;

	ret_val = 0;
	i = 1;
	while (*(argvs + i))
	{
		if (check_valid_argv(*(argvs + i)) == 1)
		{
			if (is_env_var_found(*(argvs + i), *envs_list))
				unset_env_var(*(argvs + i), envs_list);
			if (*envs_list == NULL)
				break ;
		}
		else
			ret_val = print_error("unset", *(argvs + i), NO_VALID_IDF, 1);
		i++;
	}
	return (ret_val);
}
