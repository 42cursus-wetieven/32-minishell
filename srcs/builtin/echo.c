#include "minishell.h"


int	verif_option_n(char **argv)
{
	int	i;

	i = 1;
	while (argv[i])
	{
		if (ft_strncmp(argv[i], "-n", 2) == 0)
			i++;
		else
			break ;
	}
	return (i);
}

int	do_echo(char **argv, t_node **envp)
{
	int	i;
	int	new_line;

	(void)envp;
	i = verif_option_n(argv);
	new_line = i;
	while (*(argv + i))
	{
		ft_putstr_fd(*(argv + i), STDOUT_FILENO);
		if (*(argv + i + 1))
			ft_putchar_fd(' ', STDOUT_FILENO);
		i++;
	}
	if (new_line == 1)
		ft_putchar_fd('\n', STDOUT_FILENO);
	return (0);
}
