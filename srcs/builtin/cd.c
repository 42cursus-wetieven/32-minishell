#include <limits.h>
#include "environment.h"
#include "builtin.h"

void	update_pwds(t_node *envs_list)
{
	t_node	*pwd;
	t_node	*old_pwd;
	char	buff[PATH_MAX];

	pwd = return_env_var("PWD", envs_list);
	old_pwd = return_env_var("OLDPWD", envs_list);
	if (!pwd)
	{
		if (old_pwd)
			change_env_content_val(old_pwd->ctnt, NULL);
	}
	else
	{
		if (old_pwd)
			change_env_content_val(old_pwd->ctnt, ((t_env *)pwd->ctnt)->value);
		change_env_content_val(pwd->ctnt, getcwd(buff, PATH_MAX));
	}
}

int	cd_root_dir(t_node *envs_list)
{
	t_node	*home;
	t_env	*env_content;

	home = return_env_var("HOME", envs_list);
	if (!home)
		return (print_error("cd", NULL, "HOME not set", 1));
	env_content = (t_env *)home->ctnt;
	if (chdir(env_content->value) == -1)
		return (print_error("cd", env_content->value, strerror(errno), 1));
	update_pwds(envs_list);
	return (0);
}

int	do_cd(char **argv, t_node **envp)
{
	t_node	*envs_list;

	envs_list = *envp;
	if (!*(argv + 1) || ft_strcmp(*(argv + 1), "~") == 0)
		return (cd_root_dir(envs_list));
	else
	{
		if (chdir(*(argv + 1)) == -1)
			return (print_error("cd", *(argv + 1), strerror(errno), 1));
		update_pwds(envs_list);
		return (0);
	}
}
