#include "data_access.h"
#include "data_free.h"
#include "builtin.h"

int	do_exit(char **argv, t_node **envp)
{
	(void)argv;
	(void)envp;

	vctr_exit(shell()->prompt);
	free_syntactic_node(&shell()->mainline);
	free_lst(&shell()->env, free_env_content);
	vctr_exit(shell()->probe.ctxt.lex_stk);
	vctr_exit(shell()->probe.ctxt.syn_stk);
	vctr_exit(shell()->probe.word);
	vctr_exit(shell()->probe.arg->v);
	free(shell()->probe.arg);
	exit(CLEAR);
	return (CLEAR);
}

