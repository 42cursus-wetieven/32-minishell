#include <limits.h>
#include "builtin.h"

int	do_pwd(char **argv, t_node **envp)
{
	char	buff[PATH_MAX];

	(void)argv;
	(void)envp;
	if (!getcwd(buff, PATH_MAX))
		return (print_error("pwd", NULL, strerror(errno), 1));
	ft_dprintf(STDOUT_FILENO, "%s\n", buff);
	return (0);
}
