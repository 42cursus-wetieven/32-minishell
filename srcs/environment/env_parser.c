#include "data_access.h"
#include "data_free.h"
#include "environment.h"

char	*return_var_value(char *name)
{
	t_node *var_node;

	var_node = return_env_var(name, shell()->env);
	if (var_node)
		return (((t_env *)var_node->ctnt)->value);
	else
		return (NULL);
}

void	change_env_content_val(t_env *env_content, char *new_value)
{
	if (env_content->value)
		free(env_content->value);
	if (new_value)
		env_content->value = ft_strdup(new_value);
	else
		env_content->value = NULL;
}

void	swap_env_content(t_env *curr, t_env *next)
{
	t_env	temp;

	temp.name = curr->name;
	temp.value = curr->value;
	curr->name = next->name;
	curr->value = next->value;
	next->name = temp.name;
	next->value = temp.value;
}

t_env	*init_env_content(char *name, char *value)
{
	t_env	*env_content;

	env_content = (t_env *)malloc(sizeof(t_env));
	if (!env_content)
		return (NULL);
	if (name)
		env_content->name = ft_strdup(name);
	else
		env_content->name = NULL;
	if (value)
		env_content->value = ft_strdup(value);
	else
		env_content->value = NULL;
	return (env_content);
}

int	parse_envs_list(char **envp, t_node **envs_list)
{
	t_env	*env_content;
	char	**str_split;
	char	*temp_holder;

	*envs_list = NULL;
	while (*envp)
	{
		str_split = ft_split(*envp, '=');
		if (!str_split)
			return (0);
		if (ft_strcmp(*(str_split + 0), "OLDPWD") == 0)
			env_content = init_env_content(*(str_split + 0), NULL);
		else if (ft_strcmp(*(str_split + 0), "SHLVL") == 0)
		{
			temp_holder = ft_itoa(ft_atoi(*(str_split + 1)) + 1);
			env_content = init_env_content(*(str_split + 0), temp_holder);
			free(temp_holder);
		}
		else
			env_content = init_env_content(*(str_split + 0), *(str_split + 1));
		lst_queue(envs_list, new_node(env_content));
		free_double_pointer(str_split);
		envp++;
	}
	return (1);
}
