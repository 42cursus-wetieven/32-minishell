#include "environment.h"

char	**envs_list_to_chars(t_node *envs_list)
{
	char	**argvs;
	char	*temp_buff;
	int		size;
	int		i;

	size = lst_size(envs_list);
	i = 0;
	argvs = (char **)malloc(sizeof(char *) * (size + 1));
	if (!argvs)
		return (NULL);
	while (i < size)
	{
		temp_buff = ft_strjoin(((t_env *)envs_list->ctnt)->name, "=");
		*(argvs + i) = ft_strjoin(temp_buff, ((t_env *)envs_list->ctnt)->value);
		free(temp_buff);
		i++;
		envs_list = envs_list->next;
	}
	*(argvs + size) = NULL;
	return (argvs);
}

t_node	*dup_envs_list(t_node *envs_list)
{
	t_node	*envs_copy;
	t_env	*env_content;

	envs_copy = NULL;
	while (envs_list)
	{
		env_content = (t_env *)envs_list->ctnt;
		lst_queue(&envs_copy, new_node(init_env_content(
					env_content->name, env_content->value)));
		envs_list = envs_list->next;
	}
	return (envs_copy);
}

t_node	*sort_envs_list(t_node *envs_list)
{
	t_node	*envs_sorted;
	t_node	*curr_env;
	t_node	*next_env;

	envs_sorted = dup_envs_list(envs_list);
	curr_env = envs_sorted;
	while (curr_env)
	{
		next_env = curr_env->next;
		while (next_env)
		{
			if (((t_env *)next_env->ctnt)->name && ft_strcmp(((t_env *)curr_env->ctnt)->name,
					((t_env *)next_env->ctnt)->name) > 0)
				swap_env_content(curr_env->ctnt, next_env->ctnt);
			next_env = next_env->next;
		}
		curr_env = curr_env->next;
	}
	return (envs_sorted);
}

int	is_env_var_found(char *var_name, t_node *envs_list)
{
	t_node	*temp_env;
	t_env	*env_content;

	temp_env = envs_list;
	while (temp_env)
	{
		env_content = (t_env *)temp_env->ctnt;
		if (ft_strcmp(env_content->name, var_name) == 0)
			return (1);
		temp_env = temp_env->next;
	}
	return (0);
}

t_node	*return_env_var(char *var_name, t_node *envs_list)
{
	t_env	*env_content;

	while (envs_list)
	{
		env_content = (t_env *)envs_list->ctnt;
		if (ft_strcmp(env_content->name, var_name) == 0)
			return (envs_list);
		envs_list = envs_list->next;
	}
	return (NULL);
}
