#include "data_access.h"
#include "pipes.h"
#include "redirections.h"
#include "heredocs.h"
#include "exec.h"
#include "testing.h" //TESTING
#include "launcher.h"

#include <stdio.h>
static t_error	apply_redirs(t_asn *node)
{
	t_rdir	*remaining;

	open_redirs(node->rdirs);
	connect_redirs(node, node->rdirs);
	if (prune_heredocs(node->rdirs, node->tokn) != CLEAR)
		return (MEM_ALLOC);
	remaining = vctr_tail(node->rdirs);
	if (remaining && remaining->type == HDOC)
	{
		node->hdoc.active = true;
		node->hdoc.eof = remaining->eof;
		if (pipe(node->hdoc.tailpipe) != CLEAR)
			return (MEM_ALLOC);
		if (node->fd.in != STDIN_FILENO && close(node->fd.in) != CLEAR)
			return (FD_CLOSING);
		node->fd.in = node->hdoc.tailpipe[READ_STDIN];
		/* print_syntactic_node(node, "@ prune here_docs"); // TESTING */
	}
	return (CLEAR);
}

static t_error	exec_node(void *node_addr)
{
	t_error	error;
	t_asn	*node;
	static int entries;
	static int	index;

	error = CLEAR;
	node = node_addr;
	if (node->tokn <= COMM && node->rdirs)
		error = apply_redirs(node);
	if (!error && node->tokn == COMM)
	{
		if (!entries)
			entries = 1;
		if (!index)
			index = 0;
		error = exec_cmd(node, false, entries, index);
		index++;
	}
	else if (!error && node->tokn >= PIPE)
	{
		entries = node->grp->entries;
		error = vctr_iter(node->grp, exec_node);
	}
	return (error);
}


t_error	run_ass(t_shell *shell)
{
	t_error	error;
	t_asn	*last_mainline_node;

	last_mainline_node = vctr_tail(shell->mainline.grp);
	if (shell->mainline.grp->entries == 1 && last_mainline_node->tokn == COMM)
	{
		if (last_mainline_node->rdirs)
		{
			error = apply_redirs(last_mainline_node);
			if (error)
				return (error);
		}
		return (exec_cmd(last_mainline_node, true, 1, 0));
	}
	if (track_and_link_pipes(&shell->mainline) != CLEAR)
		return (MEM_ALLOC);
	return (exec_node(&shell->mainline));
}
