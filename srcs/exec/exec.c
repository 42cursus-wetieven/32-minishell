#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>
#include <stdio.h>
#include "environment.h"
#include "err_mgmt.h"
#include "builtin.h"
#include "data_free.h"
#include "sig_handlers.h"
#include "pipes.h"
#include "testing.h" //TESTING
#include "launcher.h"
#include "exec.h"
#include "data_access.h"

bool	is_executable(char *cmd_path)
{
	struct stat	buff;

	if (stat(cmd_path, &buff) == 0 && (buff.st_mode & S_IXUSR))
		return (true);
	return (false);
}

static t_error	cmd_path(char **cmd_path, t_node *envs_list, char *cmd_name)
{
	t_node	*path_var;
	char	**pathvar_split;
	char	*cmd_dir;
	int		i;

	if (is_executable(cmd_name))
	{
		*cmd_path = ft_strdup(cmd_name);
		return (CLEAR);
	}
	path_var = return_env_var("PATH", envs_list);
	if (!path_var)
		return (print_error(cmd_name, NULL, "No such file or directory", 0));
	pathvar_split = ft_split(((t_env *)path_var->ctnt)->value, ':');
	if (!pathvar_split)
		return (MEM_ALLOC);
	i = 0;
	while (*(pathvar_split + i))
	{
		cmd_dir = ft_strjoin(*(pathvar_split + i), "/");
		*cmd_path = ft_strjoin(cmd_dir, cmd_name);
		free(cmd_dir);
		if (is_executable(*cmd_path) == 1)
		{
			free_double_pointer(pathvar_split);
			return (CLEAR);
		}
		free(*cmd_path);
		i++;
	}
	free_double_pointer(pathvar_split);
	return (PARSE);
}

static	void	wait_child(int pid)
{
	bool	got_sig;
	int		sig;
	/* int		err_no; */

	got_sig = false;
	waitpid(pid, &pid, 0);
	if (WIFEXITED(pid))
		g_err_no = WEXITSTATUS(pid);
	if (!got_sig && WIFSIGNALED(pid))
	{
		got_sig = true;
		sig = WTERMSIG(pid);
		g_err_no = 128 + sig;
	}
	while (waitpid(-1, &pid, WUNTRACED) != -1)
	{
		if (WIFEXITED(pid))
			g_err_no = WEXITSTATUS(pid);
		if (!got_sig && WIFSIGNALED(pid))
		{
			got_sig = true;
			sig = WTERMSIG(pid);
			g_err_no = 128 + sig;
		}
	}
	/* if (err_no != -1) */
	/* 	g_err_no = err_no; */
}

static void		cleanup_grp(t_asn *node, int entries, int index)
{
	int	i;
	t_asn	*next;

	i = index;
	next = node;
	while (i-- > 0)
		next--;
	while (i < entries - 1)
	{
		if (i != index && i != index - 1)
		{
			if (next->fd.out != 1)
				close(next->fd.out);
			if (next->fd.in)
				close(next->fd.in);
		}
		i++;
		next++;
	}
}

static t_error	child_proc(t_asn *node, t_builtin builtin, char *path, int index, int entries)
{
	t_error	error;
	char	**env;

	shell()->in_fork = true;
	error = CLEAR;
	interrupt_fork_signals();
	if (entries > 2)
		cleanup_grp(node, entries, index);
	if (dup2(node->fd.in, STDIN_FILENO) == ERROR)
		exit(ERROR);
	if (!error && node->fd.in != STDIN_FILENO && close(node->fd.in) == ERROR)
		exit(ERROR);
	if (!error && dup2(node->fd.out, STDOUT_FILENO) == ERROR)
		exit(ERROR);
	if (!error && node->fd.out != STDOUT_FILENO && close(node->fd.out) == ERROR)
		exit(ERROR);
	if (node->cmd.tailpipe[0] && close(node->cmd.tailpipe[0]) == -1)
		exit(ERROR);
	env = envs_list_to_chars(node->cmd.arg->envp);
	if (builtin)
		error = (builtin)(node->cmd.arg->v->data, &node->cmd.arg->envp);
	else
		error = execve(path, node->cmd.arg->v->data, env);
	free(path);
	free_double_pointer(env);
	do_exit(NULL, NULL);
	return (CLEAR);
}

static	void	close_pipes(t_asn *node)
{
	if (node->first)
	{
		close(node->cmd.tailpipe[1]);
		shell()->fd = node->cmd.tailpipe[0];
	}
	else if (node->cmd.tailpipe[1] == LAST_CMD)
	{
		if (shell()->fd)
			close(shell()->fd);
		if (node->cmd.tailpipe[1])
			close(node->cmd.tailpipe[1]);
		if (node->cmd.tailpipe[0])
			close(node->cmd.tailpipe[0]);
	}
	else
	{
		if (shell()->fd)
		{
			close(shell()->fd);
			shell()->fd = node->cmd.tailpipe[0];
		}
		if (node->cmd.tailpipe[1])
			close(node->cmd.tailpipe[1]);
	}
}

t_error	exec_cmd(t_asn *node, bool lone, int entries, int index)
{
	t_error		error;
	t_builtin	builtin;
	char		*path;
	int			pid;
	int			builtin_index;

	path = NULL;
	builtin = is_builtin(*(char **)node->cmd.arg->v->data, &builtin_index);
	if (lone && builtin && builtin_index <= EXI)
		return ((builtin)(node->cmd.arg->v->data, &node->cmd.arg->envp));
	if (!builtin)
	{
		error = cmd_path(&path, node->cmd.arg->envp,
				*(char **)node->cmd.arg->v->data);
		if (error)
			return (main_p_err(*(char **)node->cmd.arg->v->data,
					"command not found", 127, PARSE));
	}
	if (node->hdoc.active)
		if (prompt_heredoc(node, node->hdoc.eof, node->hdoc.active) != CLEAR)
			return (MEM_ALLOC);
	pid = fork();
	if (pid == -1)
		return (ERROR);
	if (pid == 0)
		child_proc(node, builtin, path, index, entries);
	close_pipes(node);
	signal(SIGINT, during_fork);
	signal(SIGQUIT, SIG_IGN);
	if (path)
		free(path);
	if (lone || node->cmd.tailpipe[1] == LAST_CMD)
		wait_child(pid);
	return (CLEAR);
}
