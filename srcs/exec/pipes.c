#include "testing.h" //TESTING
#include "pipes.h"

static t_error	link_pipe(t_asn *node)
{
	size_t	i;
	t_asn	*prec;
	t_asn	*post;

	i = 0;
	while (i < node->grp->entries - 1)
	{
		prec = vctr_entry(node->grp, i);
		if (i == 0)
			prec->first = 1;
		if (pipe(prec->cmd.tailpipe) != CLEAR)
			return (MEM_ALLOC);
		if (prec->fd.out == STDOUT_FILENO)
			prec->fd.out = prec->cmd.tailpipe[WRIT_STDOU];
		else
			if (close(prec->cmd.tailpipe[WRIT_STDOU]) == ERROR)
				return (FD_CLOSING);
		i++;
		post = vctr_entry(node->grp, i);
		if (post->fd.in == STDIN_FILENO)
			post->fd.in = prec->cmd.tailpipe[READ_STDIN];
		else
			if (close(prec->cmd.tailpipe[READ_STDIN]) == ERROR)
				return (FD_CLOSING);
	}
	post->cmd.tailpipe[WRIT_STDOU] = LAST_CMD;
	return (CLEAR);
}

t_error	track_and_link_pipes(void *node_addr)
{
	t_error	error;
	t_asn	*node;

	node = node_addr;
	if (node->tokn <= COMM)
		return (CLEAR);
	else if (node->tokn == PIPE)
		error = link_pipe(node);
	else
		error = vctr_iter(node->grp, track_and_link_pipes);
	return (error);
}
