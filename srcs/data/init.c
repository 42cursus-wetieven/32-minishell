/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/01 20:29:41 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/01 20:29:46 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "data_access.h"
#include "context_stack.h"
#include "data_init.h"

void	init_node(t_asn *node)
{
	node->tokn = BLNK;
	node->fd.in = STDIN_FILENO;
	node->fd.out = STDOUT_FILENO;
	node->rdirs = NULL;
	node->hdoc.active = false;
}

t_error	new_grouping(t_probe *prb, t_asn *parent, t_syntagma type)
{
	t_asn	new_container;
	t_vctr	*new_group;

	if (vctr_init(&new_group, sizeof(t_asn), 2) != CLEAR)
		return (MEM_ALLOC);
	new_container.tokn = type;
	new_container.grp = new_group;
	if (vctr_push(parent->grp, &new_container) != CLEAR)
		return (MEM_ALLOC);
	if (add_syn_layer(&prb->ctxt, vctr_tail(parent->grp)) != CLEAR)
		return (MEM_ALLOC);
	return (CLEAR);
}

t_error	context_stk_init(t_ctxt *ctxt)
{
	if (!ctxt->lex_stk)
		if (vctr_init(&ctxt->lex_stk, sizeof(char), 4) != CLEAR)
			return (MEM_ALLOC);
	if (!ctxt->syn_stk)
		if (vctr_init(&ctxt->syn_stk, sizeof(t_asn **), 4) != CLEAR)
			return (MEM_ALLOC);
	return (CLEAR);
}

t_error	new_blank_word(t_probe *probe)
{
	t_vctr	*blank_word;

	if (probe->word)
		return (vctr_recycle(probe->word, NULL));
	if (vctr_init(&blank_word, sizeof(char), 32) != CLEAR)
		return (MEM_ALLOC);
	probe->word = blank_word;
	return (CLEAR);
}

t_error	new_blank_arguments(t_arg **arg, t_probe *probe)
{
	*arg = malloc(sizeof(t_arg));
	if (!*arg)
		return (MEM_ALLOC);
	if (vctr_init(&(*arg)->v, sizeof(char **), 16) != CLEAR)
		return (MEM_ALLOC);
	(*arg)->c = 0;
	(*arg)->envp = shell()->env;
	return (new_blank_word(probe));
}
