/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   storage.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/01 20:20:24 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/02 13:15:21 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "data_init.h"
#include "tokenizing.h"
#include "context_stack.h"
#include "interpreter.h"
#include "data_storage.h"

t_error	push_node(t_probe *prb)
{
	t_arg	*new_blank_args;

	if (!prb->arg->v->entries && prb->curr.rdirs)
	{
		prb->curr.tokn = RDIR;
		new_blank_word(prb);
	}
	else if (!prb->arg->v->entries)
		return (CLEAR);
	else
	{
		prb->curr.tokn = COMM;
		prb->curr.cmd.arg = prb->arg;
	}
	if (vctr_push(prb->ctxt.syn->grp, &prb->curr) != CLEAR)
		return (MEM_ALLOC);
	if (prb->curr.tokn == COMM)
	{
		if (new_blank_arguments(&new_blank_args, prb))
			return (MEM_ALLOC);
		prb->arg = new_blank_args;
	}
	init_node(&prb->curr);
	return (CLEAR);
}

t_error	push_redir(t_asn *node, t_rdir *rdir, t_probe *prb)
{
	char	*path_or_eof;

	if (!node->rdirs)
		if (vctr_init(&node->rdirs, sizeof(t_rdir), 2) != CLEAR)
			return (MEM_ALLOC);
	path_or_eof = ft_strdup(prb->word->data);
	if (!path_or_eof)
		return (MEM_ALLOC);
	if (rdir->type == HDOC)
		rdir->eof = path_or_eof;
	else
		rdir->path = path_or_eof;
	return (vctr_push(node->rdirs, rdir));
}

t_error	push_char(t_probe *probe)
{
	if (lex_tokn(probe->ctxt.lex) != VARIABL
		&& probe->curr.hdoc.eof && **probe->ptr == '$')
		return (expdvar(probe));
	if (probe->ctxt.lex == '\\')
		exit_lex_layer(&probe->ctxt);
	if (!probe->word)
		if (new_blank_word(probe) != CLEAR)
			return (MEM_ALLOC);
	if (vctr_push(probe->word, *probe->ptr) != CLEAR)
		return (MEM_ALLOC);
	(*probe->ptr)++;
	return (CLEAR);
}

t_error	wording(t_probe *prb)
{
	char	*word;

	if (lex_tokn(prb->ctxt.lex) == VARIABL)
		 if (expdvar(prb) != CLEAR)
			 return (MEM_ALLOC);
	if (**prb->ptr != '\0' && !prb->word->entries)
	{
		while (ft_isspace(**prb->ptr))
			(*prb->ptr)++;
		return (CLEAR);
	}
	if (lex_tokn(prb->ctxt.lex) == REDIREC)
	{
		prb->active = false;
		return (CLEAR);
	}
	if (lex_tokn(prb->ctxt.lex) != HEREDOC && prb->word->entries)
	{
		word = ft_strdup(prb->word->data);
		if (!word || vctr_push(prb->arg->v, &word) != CLEAR)
			return (MEM_ALLOC);
		prb->arg->c++;
		vctr_recycle(prb->word, NULL);
	}
	return (CLEAR);
}
