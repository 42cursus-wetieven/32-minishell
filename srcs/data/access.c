#include "data_access.h"

t_shell	*shell(void)
{
	static t_shell	og_shell = {
		.prompt = NULL,
		.mainline.tokn = MAIN,
		.mainline.grp = NULL,
		.env = NULL,
		.probe.ctxt.lex_stk = NULL,
		.probe.ctxt.syn_stk = NULL,
		.probe.word = NULL,
		.probe.arg = NULL,
		.probe.curr.hdoc.eof = NULL,
		.probe.active = false,
		.in_fork = false,
	};

	return (&og_shell);
}
