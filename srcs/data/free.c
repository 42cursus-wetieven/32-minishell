/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/02 16:38:39 by wetieven          #+#    #+#             */
/*   Updated: 2022/03/02 16:38:43 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include "data_access.h"
#include "redirections.h"
#include "data_free.h"

t_error	free_redir_path(void *rdir)
{
	if (((t_rdir *)rdir)->path)
		free(((t_rdir *)rdir)->path);
	return (CLEAR);
}

t_error	free_word(void *word_addr)
{
	char	**word;

	word = word_addr;
	free(*word);
	return (CLEAR);
}

t_error	free_syntactic_node(void *node_addr)
{
	t_asn	*node;

	node = node_addr;
	if (node->tokn <= COMM)
		close_redirs(node, node->rdirs);
	if (node->tokn == COMM)
	{
		vctr_iter(node->cmd.arg->v, free_word);
		vctr_exit(node->cmd.arg->v);
		free(node->cmd.arg);
	}
	else if (node->tokn >= PIPE)
	{
		vctr_iter(node->grp, free_syntactic_node);
		vctr_exit(node->grp);
	}
	return (CLEAR);
}

t_error	free_env_content(void *content)
{
	t_env	*env_content;

	env_content = (t_env *)content;
	if (env_content->name)
		free(env_content->name);
	env_content->name = NULL;
	if (env_content->value)
		free(env_content->value);
	env_content->value = NULL;
	free(env_content);
	env_content = NULL;
	return (CLEAR);
}

void	free_double_pointer(char **args)
{
	int		i;

	i = 0;
	while (*(args + i) != NULL)
	{
		free(*(args + i));
		i++;
	}
	free(args);
}
